<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Custom CRUD</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
	<div class="container">
		<br />
		<br />
		<br />
		<div class="col-md-6 offset-3">
			<div>
				<a href="{{ url('/custom/create') }}" class="btn btn-primary">Add</a>
				<a href="{{ url('/custom') }}" class="btn btn-primary">View</a>
			</div>
			<br />
			<P> 
			 @if(session()->has('message'))
                        {{ session('message') }}
                @endif
			</P>
			   {!! Form::open(['url' => '/custom/'.$data->id,'method' => 'put' ]) !!}
			  <div class="form-group">
			    <label for="name">Furit Name</label>
			    <input type="text" name="name" value="{{ $data->name }}" class="form-control" id="name">
			  </div>
  				<button type="submit" class="btn btn-default">Submit</button>
			{!! Form::close() !!}
		</div>
	</div>
</body>
</html>