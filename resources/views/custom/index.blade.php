<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Custom CRUD</title>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
	<div class="container">
		<br />
		<br />
		<br />
		<div class="col-md-6 offset-3">
			<div>
				<a href="{{ url('/custom/create') }}" class="btn btn-primary">Add</a>
				<a href="{{ url('/custom') }}" class="btn btn-primary">View</a>
			</div>
			<br />
			   <P> 
			 @if(session()->has('message'))
                        {{ session('message') }}
                @endif
			</P>
			   <table class="table table-bordered">
			   		<thead>
			   			<tr>
			   				<th>#</th>
			   				<th>Furit Name</th>
			   				<th>Action</th>
			   			</tr>
			   		</thead>
			   		<tbody>
			   			@foreach($data as $record)
			   			<tr>
			   				<td>{{$record->id}}</td>
			   				<td>{{$record->name}}</td>
			   				<td>
								<a class="btn btn-primary" href="{{ url('/custom/'.$record->id.'/edit') }}">Edit</a>
								
								 {{ Form::open(array('url' => 'custom/' . $record->id)) }}
                    				{{ Form::hidden('_method', 'DELETE') }}
                    				{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                				{{ Form::close() }}
			   				</td>
			   			</tr>
			   			@endforeach
			   		</tbody>
			   </table>
		</div>
	</div>
</body>
</html>