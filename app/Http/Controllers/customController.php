<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Custom;
class customController extends Controller
{
    // Custom create method for HTML Form

    public function create()
    {
    	return view('custom.create');
    }

    public function store(Request $request)
    {
    	$data = [
    		'name' => $request->name
    	];
    	if(Custom::create($data)){
    		session()->flash('message', 'Data Inserted Successfully'); // message showing
            return redirect()->back();

    	}
    }

    public function index()
    {
    	$data = Custom::all();
    	return view('custom.index', compact('data'));
    }

    public function edit($id)
    {
    	$data = Custom::find($id);
    	return view('custom.edit',compact('data'));
    }

    public function update(Request $request, $id)
    {
    	$data = [
    		'name' => $request->name
    	];
    	
    	if(Custom::find($id)->update($data)){
    		session()->flash('message', 'Data Updated Successfully'); // message showing
            return redirect('/custom');

    	}
    }

    public function destroy($id)
    {
    	$delete = Custom::find($id);
    	if($delete->delete()){
    		session()->flash('message', 'Data Deleted Successfully'); // message showing
            return redirect('/custom');

    	}
    }
}
