<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Custom extends Model
{
    // According to DB field Name

    protected $fillable = ['name'];
    public $timestamps = false;
}
