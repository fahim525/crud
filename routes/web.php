<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Custome Route For Simple CRUD */


// 01. Create
Route::get('/custom/create', 'customController@create');

// 02. Store
Route::post('/custom/store', 'customController@store');

// 03. Read
Route::get('/custom', 'customController@index');

// 04. Edit
Route::get('/custom/{id}/edit', 'customController@edit');

// 04. Update
Route::put('/custom/{id}', 'customController@update');

// 05 Destroy 
Route::delete('/custom/{id}', 'customController@destroy');



